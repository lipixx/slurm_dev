/*****************************************************************************\
 * slurmdev.c - Driver for Linux Kernel.
 *****************************************************************************
 * This driver registers a driver in the kernel and creates SLURM_NUM_DEVICES
 * character devices linked to it. The devices can be opened, read and write.
 *
 * This is useful for testing, specially for Slurm GRES constraining
 * functionality.
 *
 * slurm_dev is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Written by Felip Moll <lipixx@gmail.com>
 *
 * February 2022
 */
#include <linux/vmalloc.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>

#define SLURM_MODULE_NAME "slurm"
#define SLURM_NUM_DEVICES 10

MODULE_LICENSE("GPL");

static int dev_major = 0;
static dev_t slurm_dev;
static struct class *slurm_class = NULL;
static struct cdev slurm_cdev[SLURM_NUM_DEVICES];

static int slurmdev_open(struct inode *inode, struct file *file);
static ssize_t slurmdev_read(struct file *file_ptr, char __user *user_buffer,
			     size_t count, loff_t *position);
static ssize_t slurmdev_write(struct file *file, const char __user *buf,
			      size_t count, loff_t *offset);

static struct file_operations slurm_fops = {
	.owner   = THIS_MODULE,
	.open    = slurmdev_open,
	.read    = slurmdev_read,
	.write   = slurmdev_write,
};

#ifndef KERNEL_LESS_EQ_62
static int slurmdev_uevent(const struct device *dev,
			   struct kobj_uevent_env *env)
#else
static int slurmdev_uevent(struct device *dev,
			   struct kobj_uevent_env *env)
#endif
{
	add_uevent_var(env, "DEVMODE=%#o", 0666);
	return 0;
}

static int slurmdev_open(struct inode *inode, struct file *file)
{
	return 0;
}

static ssize_t slurmdev_read(struct file *file_ptr, char __user *user_buffer,
			     size_t count, loff_t *position)
{
	char *ptr = (char*) vmalloc(count);
	memset(ptr, 0xFF, count);
	if (copy_to_user(user_buffer, ptr, count))
		return -EFAULT;
	return count;
}

static ssize_t slurmdev_write(struct file *file, const char __user *buf,
			      size_t count, loff_t *offset)
{
	return 0;
}

static int slurm_module_init(void)
{
	int rc;

	rc = alloc_chrdev_region(&slurm_dev, 0, SLURM_NUM_DEVICES, SLURM_MODULE_NAME);
	if (rc < 0) {
		printk(KERN_ERR "slurm_module_init: unable to register device.\n");
		return rc;
	}
	dev_major = MAJOR(slurm_dev);
	slurm_class = class_create(SLURM_MODULE_NAME);
	slurm_class->dev_uevent = slurmdev_uevent;

	/* Initialise the device files /dev/slurmX */
	for (int minor = 0; minor < SLURM_NUM_DEVICES; minor++) {
		cdev_init(&slurm_cdev[minor], &slurm_fops);
		slurm_cdev[minor].owner = THIS_MODULE;
		if (cdev_add(&slurm_cdev[minor], MKDEV(dev_major, minor), 1) < 0) {
			printk(KERN_ERR "slurm_module_init: unable to create devices");
		}
		if (!(IS_ERR(device_create(slurm_class, NULL, MKDEV(dev_major, minor), NULL,
					   "slurm%d", minor))))
			printk(KERN_NOTICE "registered character device with major %d minor %d\n",
			       dev_major, minor);
		else
			printk(KERN_ERR "slurm_module_init: mknod() error");
	}
	return 0;
}

static void slurm_module_exit(void)
{
    for (int minor = 0; minor < SLURM_NUM_DEVICES; minor++) {
        device_destroy(slurm_class, MKDEV(dev_major, minor));
	printk(KERN_NOTICE "/dev/slurm%d: unregistering device.\n", minor);
    }
    class_destroy(slurm_class);
    unregister_chrdev_region(slurm_dev, SLURM_NUM_DEVICES);
}

module_init(slurm_module_init);
module_exit(slurm_module_exit);
