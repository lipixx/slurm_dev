# Slurm testing devices

Kernel module to create some devices in linux to do testing.

## Description
When loaded this module will create up to SLURM_NUM_DEVICES (10 by default)
character devices in /dev/slurm[0-9], with its associated /sys/class/slurm
interfaces.

The devices can be opened, read, and written. Actually only the read does
something meaningful: it endlessly returns 0xFF, just as /dev/zero does.

We could say this is a /dev/one at the moment.

## Installation

* make clean
* make

For kernel <= 6.2 you must define CFLAGS:

* make clean
* make CFLAGS="-DKERNEL_LESS_EQ_62=1"

Need roots permissions:

* make load : Does an insmod and loads the module
* make unload : Does an rmmod to unload the module
* make install : Installs the module in the system
* make uninstall: Removes the module from the system

After a 'make install', load it permanently adding a file:

```
]# cat /etc/modules-load.d/slurm_dev.conf
slurm_dev
```
## Usage

When the module is loaded, check /proc/devices|grep slurm, /dev/slurm* and
/sys/class/slurm*. You can make use of all of these as regular devices.

## License
This code is academic and for testing purposes, licensed under GPLv3.
