TARGET_MODULE	:= slurm_dev
KERNEL      	:= /lib/modules/$(shell uname -r)/build
ARCH        	:= x86
CFLAGS     	:= -Wall -std=gnu99 -Wno-declaration-after-statement
KMOD_DIR    	:= $(shell pwd)
TARGET_PATH 	:= /lib/modules/$(shell uname -r)/kernel/drivers/char

OBJECTS := slurmdev.o

ccflags-y += $(CFLAGS)

obj-m += $(TARGET_MODULE).o

$(TARGET_MODULE)-y := $(OBJECTS)

$(TARGET_MODULE).ko:
	make -C $(KERNEL) M=$(KMOD_DIR) modules

install:
	cp $(TARGET_MODULE).ko $(TARGET_PATH)
	depmod -a

uninstall:
	rm $(TARGET_PATH)/$(TARGET_MODULE).ko
	depmod -a

load:
	insmod ./$(TARGET_MODULE).ko

unload:
	rmmod ./$(TARGET_MODULE).ko

clean:
	make -C $(KERNEL) M=$(KMOD_DIR) clean
